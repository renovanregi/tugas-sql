1. Membuat database
create database myshop;

2. Membuat table dalam database

Categories
create table categories(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> primary key(id),
    -> );

Items
create table items(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(10),
    -> stock int(10),
    -> categories_id int(10),
    -> primary key(id),
    -> foreign key(categories_id) references categories(id)
    -> );

Users
create table users(
    -> id int(10) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

3. Memasukkan data pada table

Categories
insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");

Items
insert into items(name, description, price, stock, categories_id) values
    -> ("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", 1),("Uniklooh", "baju keren dari brand ternama", "500000", "50", 2),("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", 1);

Users
insert into users(name, email, password) values
    -> ("John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");

4. Mengambil data dari database

a. Mengambil data users
select id, name, email from users;

b. Mengambil data items
select * from items where price > 1000000;
select * from items where name like "%watch";

c. Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock, items.categories_id, categories.name As kategori
    -> from items inner join categories on items.categories_id = categories.id;

5. Mengubah data dari database
update items set price = "2500000" where id = 1;